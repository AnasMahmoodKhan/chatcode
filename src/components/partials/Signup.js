import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class Signup extends Component {
  state = {
    firstname: "",
    lastname: "",
    email: "",
    password: "",
    error: [],
  };

  validation = (field) => {
      console.log(field)
    switch (field) {
      case "firstname":
        let fnFormat = /^[a-zA-Z]+$/;
        if (this.state.firstname.match(fnFormat)) {
          let error = this.state.error;
          error = error.filter((err) => err.type !== "firstname");
          console.log(error);
          this.setState({
            error,
          });
        } else {
          let error = this.state.error;
          error.push({
            type: "firstname",
            message: "Please enter your full name.",
          });
          this.setState({
            error,
          });
        }
        break;

      case "lastname":
        let lsFormat = /^[a-zA-Z]+$/;
        if (this.state.lastname.match(lsFormat)) {
          let error = this.state.error;
          error = error.filter((err) => err.type !== "lastname");
          console.log(error);
          this.setState({
            error,
          });
        } else {
          let error = this.state.error;
          error.push({
            type: "lastname",
            message: "Please enter your last name.",
          });
          this.setState({
            error,
          });
        }
        break;

      case "email":
        let mailformat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (this.state.email.match(mailformat)) {
          let error = this.state.error;
          error = error.filter((err) => err.type !== "email");
          console.log(error);
          this.setState({
            error,
          });
        } else {
          let error = this.state.error;
          error.push({ type: "email", message: "Invalid Email Format." });
          this.setState({
            error,
          });
        }
        break;

      case "password":
        let passwordFormat = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
        if (this.state.password.match(passwordFormat)) {
          let error = this.state.error;
          error = error.filter((err) => err.type !== "password");
          console.log(error);
          this.setState({
            error,
          });
        } else {
          let error = this.state.error;
          error.push({
            type: "password",
            message:
              "Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters.",
          });
          this.setState({
            error,
          });
        }
        break;

      default:
        break;
    }
  };

  render() {
    return (
      <div className="outer">
        <div className="inner">
          <form
            onSubmit={(e) => {
              e.preventDefault();
              if (this.props.socket) {
                this.props.socket.send(
                  JSON.stringify({
                    type: "SIGNUP",
                    data: {
                      firstname: this.state.firstname,
                      lastname: this.state.lastname,
                      email: this.state.email,
                      password: this.state.password,
                    },
                  })
                );
              }
            }}
          >
            <h3>Register</h3>

            <div className="form-group">
              <label>First name</label>
              <input
                name="firstname"
                value={this.state.firstname}
                onChange={(e) => this.setState({ firstname: e.target.value })}
                onBlur={(e) => {
                  this.validation(e.target.name);
                }}
                className={
                  this.state.error.find((err) => err.type === "firstname")
                    ? "form-control border-danger"
                    : "form-control"
                }
                placeholder="First name"
              />
              {this.state.error.find((err) => err.type === "firstname") ? (
                <small className="text-danger">
                  {
                    this.state.error[
                      this.state.error.findIndex(
                        (err) => err.type === "firstname"
                      )
                    ].message
                  }
                </small>
              ) : null}
            </div>

            <div className="form-group">
              <label>Last name</label>
              <input
                name="lastname"
                value={this.state.lastname}
                onChange={(e) => this.setState({ lastname: e.target.value })}
                onBlur={(e) => {
                  this.validation(e.target.name);
                }}
                className={
                  this.state.error.find((err) => err.type === "lastname")
                    ? "form-control border-danger"
                    : "form-control"
                }
                placeholder="Last name"
              />
              {this.state.error.find((err) => err.type === "lastname") ? (
                <small className="text-danger">
                  {
                    this.state.error[
                      this.state.error.findIndex(
                        (err) => err.type === "lastname"
                      )
                    ].message
                  }
                </small>
              ) : null}
            </div>

            <div className="form-group">
              <label>Email</label>
              <input
                type="email"
                value={this.state.email}
                onChange={(e) => this.setState({ email: e.target.value })}
                onBlur={(e) => {
                  this.validation(e.target.type);
                }}
                className={
                  this.state.error.find((err) => err.type === "email")
                    ? "form-control border-danger"
                    : "form-control"
                }
                placeholder="Enter email"
              />
              {this.state.error.find((err) => err.type === "email") ? (
                <small className="text-danger">
                  {
                    this.state.error[
                      this.state.error.findIndex((err) => err.type === "email")
                    ].message
                  }
                </small>
              ) : null}
            </div>

            <div className="form-group">
              <label>Password</label>
              <input
                type="password"
                value={this.state.password}
                onChange={(e) => this.setState({ password: e.target.value })}
                onBlur={(e) => {
                  this.validation(e.target.type);
                }}
                className={
                  this.state.error.find((err) => err.type === "password")
                    ? "form-control border-danger"
                    : "form-control"
                }
                placeholder="Enter password"
              />
              {this.state.error.find((err) => err.type === "password") ? (
                <small className="text-danger">
                  {
                    this.state.error[
                      this.state.error.findIndex(
                        (err) => err.type === "password"
                      )
                    ].message
                  }
                </small>
              ) : null}
            </div>

            <button type="submit" className="btn btn-dark btn-lg btn-block">
              Register
            </button>
            <p className="forgot-password text-right">
              Already registered <Link to="/login">log in?</Link>
            </p>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.auth,
  ...state.chatRed,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
