import { combineReducers } from "redux";
import auth from "./authReducer";
import chatRed from "./chatReducer";

export default combineReducers({
  auth,
  chatRed,
});
