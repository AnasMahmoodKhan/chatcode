export const setupSocket = () => {
  const socket = new WebSocket("ws://localhost:8080");
  return (dispatch) => {
    socket.onopen = () => {
      dispatch({
        type: "SETUP_SOCKET",
        payload: socket,
      });
    };
  };
};
